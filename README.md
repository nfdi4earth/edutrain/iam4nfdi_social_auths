# NFDI Social Auth

This [Python package](https://packaging.python.org/en/latest/tutorials/packaging-projects/) provides straightforward mechanisms for connecting with the community AAI solutions for NFDI IAM infrastructure ([IAM4NFDI](https://doc.nfdi-aai.de)) via the OAuth2 protocol.

It is fundamentally base on the [Social Core](https://github.com/omab/python-social-auth/tree/master) of the [Python Social Auth](https://python-social-auth.readthedocs.io/en/latest/).


## Supported Community AAI Software

A basic overview about all Community AAI Instances can be found in the [IAM4NFDI-Documentation](https://doc.nfdi-aai.de/community-aai-software/).

### Academic ID

- [main documentation](https://docs.gwdg.de/doku.php?id=en:services:general_services:academicid:start)
- [openid-configuration](https://keycloak.sso.gwdg.de/auth/realms/academiccloud/.well-known/openid-configuration)


### Unity

- [main documentation](https://hifis.net/doc/helmholtz-aai/)
- [service registration](https://hifis.net/doc/helmholtz-aai/howto-services/#service-registration)
- [endpoint urls](https://hifis.net/doc/helmholtz-aai/howto-services/#further-information-and-endpoints)

### RegApp

- [main documentation](https://www.scc.kit.edu/dienste/regapp.php)
- [openid-configuration](https://regapp.nfdi-aai.de/oidc/realms/nfdi/.well-known/openid-configuration)

## Exemplary integration

### Open edX (LMS)

This project evolved by the demand to integrate the community AAI software solutions as third party backends to a self hosted instance of the Learning Managment System (Open edX)[https://openedx.org/de/]. This django based open source software makes supports the [integration of third party authentication](https://edx.readthedocs.io/projects/edx-installing-configuring-and-running/en/latest/configuration/tpa/tpa_integrate_open/index.html).

You can have a look at the documentation on [how to integrate common OAuth2 Providers](https://edx.readthedocs.io/projects/edx-installing-configuring-and-running/en/latest/configuration/tpa/tpa_integrate_open/tpa_oauth.html#common-oauth2-providers) and [how to integrate additional OAuth2 Providers](https://edx.readthedocs.io/projects/edx-installing-configuring-and-running/en/latest/configuration/tpa/tpa_integrate_open/tpa_oauth.html#additional-oauth2-providers-advanced).

We include this package as an additional requirement:

```bash
tutor config save --append OPENEDX_EXTRA_PIP_REQUIREMENTS=git+https://git.rwth-aachen.de/nfdi4earth/architecture/nfdi_social_auth.git
```

And set the OAuth2 provider via [tutor-plugin](https://docs.tutor.edly.io/plugins/v0/gettingstarted.html) (`third-party-auth.yml`):

```yaml
name: third-party-auth
version: 1.0.0
patches:
  common-env-features: |
    ENABLE_THIRD_PARTY_AUTH: true
    ENABLE_COMBINED_LOGIN_REGISTRATION: true
  openedx-auth: |
    SOCIAL_AUTH_OAUTH_SECRETS:
      unity-oauth2: "{{ oauth.unity.secret }}"
      regapp-oauth2: "{{ oauth.regapp.secret }}"
      academicid-oauth2: "{{ oauth.academicid.secret }}"
  lms-env: |
    THIRD_PARTY_AUTH_BACKENDS:
      - "nfdi_social_auth.unity.UnityOAuth2"
      - "nfdi_social_auth.regapp.RegAppOAuth2"
      - "nfdi_social_auth.academicid.AcademicIDOAuth2"
```
