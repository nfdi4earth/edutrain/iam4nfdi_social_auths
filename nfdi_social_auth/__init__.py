"""Top-level package for IAM4NFDI social auths."""

__author__ = """Ralf Klammer"""
__email__ = 'ralf.klammer@tu-dresden.de'
__version__ = '0.1.0'
